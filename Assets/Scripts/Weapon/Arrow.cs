using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
	Rigidbody rb;
	BoxCollider bx;
	bool disableRotation;
	public float destroyTime = 5f;
	// Start is called before the first frame update
	void Start()
	{
		rb = GetComponent<Rigidbody>();
		bx = GetComponent<BoxCollider>();
		Destroy(this.gameObject, destroyTime);
	}

	private void Update()
	{
		if (!disableRotation)
			transform.rotation = Quaternion.LookRotation(rb.velocity);
	}

	private void OnCollisionEnter(Collision collision)
	{
		
		if (collision.gameObject.tag != "Player")
		{
			disableRotation = true;
			rb.isKinematic = true;
			bx.isTrigger = true;
		}


	}

	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "papan")
		{
			
			Destroy(gameObject);
			
		}
	}
	
}
