using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyScript : MonoBehaviour
{
    public FixedTouchField TouchField;
    
    // Update is called once per frame
    void Update()
    {
        var fps = GetComponent<PlayerController>();

        fps.LookAxis = TouchField.TouchDist;
    }
}
