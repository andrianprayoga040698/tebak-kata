using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	[SerializeField] Transform playerCamera = null;
	[SerializeField] float mouseSensitivity = 3.5f;
	
	float cameraPirtch = 0.0f;

	[System.Serializable]
	public class CameraSettings
	{
		[Header("Camera Settings")]
		public float zoomSpeed = 5;
		public float moveSpeed = 5;
		public float originalFieldofView = 60;
		public float zoomFieldofView = 40;
	}
	[SerializeField]
	public CameraSettings cameraSettings;

	[System.Serializable]
	public class CameraInputSttings
	{
		public string AimingInput = "Fire2";
	}
	[SerializeField]
	public CameraInputSttings inputSettings;

	Camera maincam;
	Camera UICam;

	// Start is called before the first frame update
	void Start()
	{
		maincam = Camera.main;
		UICam = maincam.GetComponentInChildren<Camera>();
	}

	// Update is called once per frame
	void Update()
	{
		UpdateMouseLook();
		zoomCamera();
	}

	void UpdateMouseLook()
	{
		Vector2 mouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

		cameraPirtch -= mouseDelta.y * mouseSensitivity;

		cameraPirtch = Mathf.Clamp(cameraPirtch, -90.0f, 90.0f);

		playerCamera.localEulerAngles = Vector3.right * cameraPirtch;

		transform.Rotate(Vector3.up * mouseDelta.x * mouseSensitivity);
	}

	void zoomCamera()
	{
		if (Input.GetButton(inputSettings.AimingInput))
		{
			maincam.fieldOfView = Mathf.Lerp(maincam.fieldOfView, cameraSettings.zoomFieldofView, cameraSettings.zoomSpeed * Time.deltaTime);
			UICam.fieldOfView = Mathf.Lerp(maincam.fieldOfView, cameraSettings.zoomFieldofView, cameraSettings.zoomSpeed * Time.deltaTime);
		}
		else
		{
			maincam.fieldOfView = Mathf.Lerp(maincam.fieldOfView, cameraSettings.originalFieldofView, cameraSettings.zoomSpeed * Time.deltaTime);
			UICam.fieldOfView = Mathf.Lerp(maincam.fieldOfView, cameraSettings.originalFieldofView, cameraSettings.zoomSpeed * Time.deltaTime);
		}
	}
}
