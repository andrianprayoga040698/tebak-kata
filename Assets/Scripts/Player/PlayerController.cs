using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CharacterController))]


public class PlayerController : MonoBehaviour
{
	[SerializeField] Transform playerCamera = null;
	[SerializeField] float mouseSensitivity = 0.5f;
	[SerializeField] bool lockCursor = true;
	public bool AktifMouse;
	[HideInInspector]
	public Vector2 LookAxis;
	
	float cameraPitch = 0.0f;

	CharacterController cc;
	Animator anim;

	

	[System.Serializable]

	public class AnimationStrings
	{
		public string aim = "aim";
		public string pullString = "pullString";
		public string fire = "fire";
	}

	[System.Serializable]
	public class InputSettings
	{
		public string loock = "Fire2";
		public string shoot = "Fire1";
	}
	[SerializeField]
	public InputSettings input;

	[Header("Aiming Settings")]
	RaycastHit hit;
	public LayerMask aimLayers;
	Ray ray;

	[Header("Spine Settings")]
	public Transform spine;
	public Vector3 spineOffset;

	[Header("Head Rotation Settings")]
	public float lookAtPoint = 2.8f;

	
	Transform mainCam;
	Camera MainCam;
	Camera UICam;
	

	
	public Bow bowScript;
	bool isAiming;

	public bool testAim;
	public static int testAimPointer = 0;
	bool isFiring;
	public static int isFiringPointer = 0;

	bool hitDetected;

	[System.Serializable]
	public class CameraSettings
	{
		[Header("Camera Settings")]
		public float zoomSpeed = 5;
		public float moveSpeed = 5;
		public float originalFieldofView = 60;
		public float zoomFieldofView = 40;
	}
	[SerializeField]
	public CameraSettings cameraSettings;

	[System.Serializable]
	public class CameraInputSttings
	{
		public string AimingInput = "Fire2";
	}
	[SerializeField]
	public CameraInputSttings inputSettings;

	

	void Start()
	{
		AktifMouse = false;
		if(lockCursor)
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false; 
		}
		
		cc = GetComponent<CharacterController>();
		anim = GetComponent<Animator>();

		mainCam = Camera.main.transform;
		MainCam = Camera.main;
		UICam = MainCam.GetComponentInChildren<Camera>();

	}

	// Update is called once per frame
	void Update()
	{
		UpdateMouseLook();
		zoomCamera();

		// isAiming = Input.GetButton(input.loock);
		if (testAim)
			isAiming = true;

		CharacterAim(isAiming);
		
		if(isAiming)
		{
			Aim();
			// CharacterPullString(Input.GetButton(input.shoot));
			// if (Input.GetButtonUp(input.shoot))
			// { 
			// 	CharacterFireArrow(); 
			// 	if(hitDetected)
			// 	{
			// 		bowScript.Fire(hit.point);
					

			// 	}
			// 	else
			// 	{
			// 		bowScript.Fire(ray.GetPoint(300f));
			// 	}
			// }

			if(isFiring)
			{
				CharacterPullString(isFiring);
				if (isFiringPointer == 1)
				{ 
					CharacterFireArrow(); 
					if(hitDetected)
					{
						bowScript.Fire(hit.point);
					}
					else
					{
						bowScript.Fire(ray.GetPoint(300f));
					}
					isFiring = false;
					CharacterPullString(isFiring);
					isFiringPointer -= 1;
					
				}
			}
		}
		else
		{
			bowScript.RemoveCrosshair();
			DisableArrow();
			Release();
		}
			
	}

	void LateUpdate()
	{
		if (isAiming)
			RotateCharacterSpine();
	}

	void UpdateMouseLook()
	{
		if(AktifMouse)
		{
			Vector2 mouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

			cameraPitch -= mouseDelta.y * mouseSensitivity;

			cameraPitch = Mathf.Clamp(cameraPitch, -90.0f, 90.0f);

			playerCamera.localEulerAngles = Vector3.right * cameraPitch;

			transform.Rotate(Vector3.up * mouseDelta.x * mouseSensitivity);
		}
		else
		{
			Vector2 mouseDelta = new Vector2(LookAxis.x, LookAxis.y);

			cameraPitch -= mouseDelta.y * mouseSensitivity;

			cameraPitch = Mathf.Clamp(cameraPitch, -90.0f, 90.0f);

			playerCamera.localEulerAngles = Vector3.right * cameraPitch;

			transform.Rotate(Vector3.up * mouseDelta.x * mouseSensitivity);
		}
		
	}

	void zoomCamera()
	{
		if (Input.GetButton(inputSettings.AimingInput))
		{
			MainCam.fieldOfView = Mathf.Lerp(MainCam.fieldOfView, cameraSettings.zoomFieldofView, cameraSettings.zoomSpeed * Time.deltaTime);
			UICam.fieldOfView = Mathf.Lerp(UICam.fieldOfView, cameraSettings.zoomFieldofView, cameraSettings.zoomSpeed * Time.deltaTime);
		}
		else
		{
			MainCam.fieldOfView = Mathf.Lerp(MainCam.fieldOfView, cameraSettings.originalFieldofView, cameraSettings.zoomSpeed * Time.deltaTime);
			UICam.fieldOfView = Mathf.Lerp(UICam.fieldOfView, cameraSettings.originalFieldofView, cameraSettings.zoomSpeed * Time.deltaTime);
		}
	}

	public void CharacterAim(bool aiming)
	{
		anim.SetBool("aim" , aiming);
	}

	public void CharacterPullString(bool pull)
	{
		anim.SetBool("pullString", pull);
	}

	public void CharacterFireArrow()
	{
		anim.SetTrigger("fire");
	}

	//aiming dan mengirim raycas ke target
	void Aim()
	{
		Vector3 camPosition = mainCam.position;
		Vector3 dir = mainCam.forward;

		ray = new Ray(camPosition, dir);
		if(Physics.Raycast(ray, out hit, 500f, aimLayers))
		{
			hitDetected = true;
			Debug.DrawLine(ray.origin, hit.point, Color.green);
			bowScript.ShowCrosshair(hit.point);
		}
		else
		{
			hitDetected = false;
			bowScript.RemoveCrosshair();
		}
	}

	void RotateCharacterSpine()
	{
		spine.LookAt(ray.GetPoint(50));
		spine.Rotate(spineOffset);
	}
	
	public void Pull()
	{
		bowScript.PullString();
	}

	public void EnableArrow()
	{
		bowScript.PickArrow();
	}

	public void DisableArrow()
	{
		bowScript.DisableArrow();
	}

	public void Release()
	{
		bowScript.ReleaseString();
	}
	public void AimSet()
	{
		testAimPointer += 1;
		if( testAimPointer == 1)
		{
			testAim = true;
		}

		if( testAimPointer == 2)
		{
			testAim = false;
			testAimPointer = 0;
		}
	}

	public void pointerDown()
	{
		isFiring = true;
	}
	public void pointerUp()
	{
		isFiringPointer += 1;
	}

}
