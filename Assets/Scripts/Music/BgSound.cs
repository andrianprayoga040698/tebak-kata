using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class BgSound : MonoBehaviour
{
    public static BgSound BgInstantMusic;

    private void Awake()
    {
        if(BgInstantMusic != null && BgInstantMusic != this)
        {
            Destroy(this.gameObject);
            return;
        }
        BgInstantMusic = this;
        DontDestroyOnLoad(this);
    }
}
