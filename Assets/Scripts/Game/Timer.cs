using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour
{
    public TextMeshProUGUI TextTimer;
    public float Waktu = 100;
    public bool TimerAktif = true;
    public GameObject LayarKalah;

    void SetText()
    {
        int Menit = Mathf.FloorToInt(Waktu / 60);
        int Detik = Mathf.FloorToInt(Waktu % 60);
        TextTimer.text = Menit.ToString("00") +":"+ Detik.ToString("00");
    }
  
    float s;
    // Update is called once per frame
    private void Update()
    {
        if(TimerAktif)
        {
            s += Time.deltaTime;
            if(s >= 1)
            {
                Waktu --;
                s = 0;
            }
        }
        
        if(TimerAktif && Waktu < 0)
        {
            Debug.Log("Waktu Habis");
            LayarKalah.SetActive(true);
            TimerAktif = false;
        }

        SetText();
    }
}
