using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour
{
    public GameObject heart1, heart2, heart3;
    public GameObject player, layarKalah;
    public static int healt;
    public int gameRetry;
    // Start is called before the first frame update
    void Start()
    {
        healt = 3;
        heart1.gameObject.SetActive(true);
        heart2.gameObject.SetActive(true);
        heart3.gameObject.SetActive(true);
        player.gameObject.SetActive(true);
        layarKalah.gameObject.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        switch(healt)
        {
            case 3:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                break;
            case 2:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(false);
                break;
            case 1:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(false);
                heart3.gameObject.SetActive(false);
                break;
            case 0:
                heart1.gameObject.SetActive(false);
                heart2.gameObject.SetActive(false);
                heart3.gameObject.SetActive(false);
                player.gameObject.SetActive(false);
                layarKalah.gameObject.SetActive(true);
                // Time.timeScale = 0f;
                Debug.Log("You Lose");
                break;
        }
    }

    public void HomeGame()
    {
        SceneManager.LoadScene(0);
        
    }

    public void RetryGame()
    {
        SceneManager.LoadScene(gameRetry);
        
    }

    public void NextGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        
    }
}
