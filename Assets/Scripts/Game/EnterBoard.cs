using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterBoard : MonoBehaviour
{
    public GameObject gameManager;

    void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "arrow")
		{
				gameManager.GetComponent<NextQuiz>().SendAnswer();
		}
	}
}
