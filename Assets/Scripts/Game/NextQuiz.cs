using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class NextQuiz : MonoBehaviour
{
    public GameObject LayarBenar;
    public GameObject TeksLayarBenar;
    public GameObject TeksSalah;
    
	public GameObject WordArray;
   
    public string Key;

    static int NO_OF_CHARS = 256;

    string CountAnswer;
    bool benar = false;
    
   

    // Start is called before the first frame update
    void Start()
    {
       LayarBenar.gameObject.SetActive(false);
       TeksSalah.gameObject.SetActive(false);
    }

   

    public void SetAnswer(string nama)
    {
        
        CountAnswer = CountAnswer + nama;
        WordArray.GetComponent<TextMeshProUGUI>().text = CountAnswer;
        
    }

    public void SendAnswer()
    {
        Debug.Log(CountAnswer);
        char[] txt = Key.ToCharArray();
		char[] pat = CountAnswer.ToCharArray();
		search(txt, pat);

        
    }

    static int max(int a, int b) { return (a > b) ? a : b; }
	static void badCharHeuristic(char[] str, int size, int[] badchar)
	{
		int i;

		
		for (i = 0; i < NO_OF_CHARS; i++)
			badchar[i] = -1;

		
		for (i = 0; i < size; i++)
			badchar[(int)str[i]] = i;
	}

	 void search(char[] txt, char[] pat)
	{
		int m = pat.Length;
		int n = txt.Length;

		int[] badchar = new int[NO_OF_CHARS];

		
		badCharHeuristic(pat, m, badchar);

		int s = 0; 
		while (s <= (n - m))
		{
			int j = m - 1;

			
			while (j >= 0 && pat[j] == txt[s + j])
				j--;

			if (j < 0)
			{
				Debug.Log("Patterns di Temukan pada index: " + s);
                LayarBenar.gameObject.SetActive(true);
                TeksLayarBenar.GetComponent<TextMeshProUGUI>().text = CountAnswer;
                benar = true;
                Invoke("TrueAnswer",2);
                s += (s + m < n) ? m - badchar[txt[s + m]] : 1;				
								
			}

			else
            {
                s += max(1, j - badchar[txt[s + j]]);
				Debug.Log("Pattern geser ke index "+ s);
            }	
		}

        if(benar != true)
        {
            Debug.Log("Jawaban Salah!");
            GameManagerScript.healt -= 1;
            TeksSalah.gameObject.SetActive(true);
            Invoke("ResetAnswer", 2);
        }
		
	}

    public void ResetAnswer()
    {
        TeksSalah.gameObject.SetActive(false);
        WordArray.GetComponent<TextMeshProUGUI>().text = "_";
        CountAnswer = "";
    }

 

    public void TrueAnswer()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        
    }
}
