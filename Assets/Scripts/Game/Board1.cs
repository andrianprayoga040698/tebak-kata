using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board1 : MonoBehaviour
{
    public string nama;

    public GameObject gameManager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "arrow")
		{
			// Debug.Log(nama);
			CallSetAnswer();
            Destroy(gameObject);			
		}
	}

    public void CallSetAnswer()
	{
		gameManager.GetComponent<NextLevel>().SetAnswer(nama);
	}
}
